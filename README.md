# Quick start reference

The below setup allows Gitlab workers to push to your platform.sh
repository. Therefore, gitlab needs to have its own ssh keypair. There are
security implications to this, especially if you do not own the GitLab
instance. Therefore be sure to create a dedicated GitLab user in
Platform.sh, and give it a dedicated key pair. For more compartmentalization,
be sure that every project you manage at Platform.sh has its own GitLab user.

## Steps:

1. Clone this repository.
2. Set up a new gitlab project. Add the repo as a new remote to this 
   project.
3. Create platform.sh profile ONLY for Gitlab.
4. Create a key pair to be used ONLY by Gitlab, and provide the public key
   to Platform.sh in `Account Settings > SSH keys`
5. Create a Platform.sh API token for the Gitlab user user in 
   `Account settings > API Tokens`
6. Create a platform.sh project, and give the Gitlab user the access to it.
7. In Gitlab, in Settings > CI / CD variables provide following variables:
   * *PLATFORMSH_CLI_TOKEN* = the api token you generated in step 5
   * *PSH_PROJECT_ID* = the alphanumeric ID of the project in platform.sh
   * *SSH_KEY* = the PRIVATE key you generated in the step 4
   * *SSH_KEY_PUB* = the public key generated in the step 4
   * *SSH_KNOWN_HOSTS* = the below example is a known hosts record for the eu-2 region. Use your local
     known hosts file for the other PSH regions.
   
```
ssh.eu-2.platform.sh ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFKM3dQWu2/aCXDhZVSSHEQskK+6pN+snPS7XSfd9TgbmhUKYhp22bHsA/YAiUh/V1CB/hsGQLByvPNt/J0lRAn8in6VoCOad6NRZErs315FsmiU2jH+bcS4imKj3ayje81cn9qJPJpe5Jg6YIqz00l/XjUiz58ehFJE+xkqiKmZ46uVKH9z6jC46WQj5ia4tZYGpF5l0fAzfzq7IXhz8PLGfrDYJt5O28I1CerJ8tCX2zdOJ9I/urNuZ/FlnijDtZq3LP6/lmWOXoxOFaDKHhCwaRnlgonMskIZgnh5wOxi5QqLpZ+D0KX3x8iDVO1bstf/HKrOPSen0i8QQyj+Bd
git.eu-2.platform.sh ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFKM3dQWu2/aCXDhZVSSHEQskK+6pN+snPS7XSfd9TgbmhUKYhp22bHsA/YAiUh/V1CB/hsGQLByvPNt/J0lRAn8in6VoCOad6NRZErs315FsmiU2jH+bcS4imKj3ayje81cn9qJPJpe5Jg6YIqz00l/XjUiz58ehFJE+xkqiKmZ46uVKH9z6jC46WQj5ia4tZYGpF5l0fAzfzq7IXhz8PLGfrDYJt5O28I1CerJ8tCX2zdOJ9I/urNuZ/FlnijDtZq3LP6/lmWOXoxOFaDKHhCwaRnlgonMskIZgnh5wOxi5QqLpZ+D0KX3x8iDVO1bstf/HKrOPSen0i8QQyj+Bd
```
