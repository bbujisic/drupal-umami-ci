<?php

namespace Drupal\hello\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\hello\Hello;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hello controller.
 */
class HelloController extends ControllerBase {

  private $helloSayer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('hello.hello_sayer'));
  }

  /**
   * A constructor.
   */
  public function __construct(Hello $helloSayer) {
    $this->helloSayer = $helloSayer;
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => $this->helloSayer->sayHello('world'),
    ];
  }

}
