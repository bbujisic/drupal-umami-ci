<?php

namespace Drupal\Tests\hello\Unit;

use Drupal\hello\Hello;
use Drupal\Tests\UnitTestCase;

/**
 * Hello unit test.
 */
class HelloTest extends UnitTestCase {

  protected $unit;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    $this->unit = new Hello();
  }

  /**
   * Test hello-saying method.
   */
  public function testHello() {
    $this->assertEquals('Hello world', $this->unit->sayHello('world'), 'Concatenation done right.');
  }

}
