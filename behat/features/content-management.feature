Feature: Hello page
When I visit the website
As an anonymous
I should be able to see a hello page

@api
  Scenario: An anonymous should see the hello page
    Given I am an anonymous user
    When I go to "hello"
    Then I should see "Hello world"